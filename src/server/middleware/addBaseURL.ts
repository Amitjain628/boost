import appConstants from '@common/constants/appConstants';

export default (
  langugageCode: string = `${process.env.LANGUAGE_CODE}`,
  countryCode: string = `${process.env.COUNTRY_CODE}`,
  translationKey: string = `${process.env.TRANSLATION_API_KEY}`,
  configurationKey: string = `${process.env.CONFIGURATION_API_KEY}`,
  contentLanguage: string = `${process.env.CONTENT_LANGUAGE}`,
  channel: string = `${process.env.CHANNEL}`
) => {
  appConstants.TRANSLATION_BASE_URL = `${
    process.env.CMS_BASE_URL
  }/${translationKey}/${langugageCode}`;
  appConstants.CONFIGURATION_BASE_URL = `${
    process.env.CMS_BASE_URL
  }/${configurationKey}`;
  appConstants.CONFIGURATION_API_KEY = configurationKey;
  appConstants.ESHOP_BASE_URL = `${process.env.ESHOP_BASE_URL}`;
  appConstants.COUNTRY_CODE = countryCode;
  appConstants.LANGUAGE_CODE = langugageCode;
  appConstants.CONTENT_LANGUAGE = contentLanguage;
  appConstants.CHANNEL = channel;
  appConstants.RUM_SERVER_URL = process.env.RUM_SERVER_URL || '';
  appConstants.RUM_SERVER_NAME = process.env.RUM_SERVER_NAME || '';
  appConstants.RUM_VERSION = process.env.RUM_VERSION || '';
};
