import React, { FunctionComponent } from 'react';
import Loadable, { LoadingComponentProps } from 'react-loadable';
import { APP_ROUTES } from '@src/common/constants/appConstants';

const loading: FunctionComponent<LoadingComponentProps> = () => null;

const NotFound = Loadable({
  loading,
  loader: () => import(/* webpackChunkName: 'not-found' */ './routes/notFound')
});

const Home = Loadable({
  loading,
  loader: () => import(/* webpackChunkName: 'not-found' */ './routes/home')
});

const routes: IRoutes[] = [
  {
    path: '/',
    basePath: '/',
    exact: true,
    component: Home,
    isProtected: false
  },
  {
    path: APP_ROUTES.NOT_FOUND,
    basePath: APP_ROUTES.NOT_FOUND,
    exact: true,
    component: NotFound,
    isProtected: false
  }
];

export default routes;

export interface IRoutes {
  childRoutes?: IRoutes[];
  basePath?: string;
  path?: string;
  exact?: boolean;
  // tslint:disable-next-line:no-any
  component?: React.ComponentType<any>;
  isProtected: boolean;
  loadData?(): Generator;
}
