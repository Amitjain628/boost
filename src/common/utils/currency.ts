import APP_CONSTANTS from '@common/constants/appConstants';
import { currencyDetails } from '@common/constants/currencyDetails';
// TODO: locale from which store ?
// const getCurrencyLocale = () =>
// store.getState().configuration.cms_configuration.global.currency.locale;

export const formatCurrency = (currency: number, currencyCode: string) => {
  return `${getFormatedCurrencyValue(currency)} ${
    currencyDetails[currencyCode].symbol
  }`;
};

export const getFormatedCurrencyValue = (currency: number) => {
  if (String(currency).includes('.')) {
    return new Intl.NumberFormat(APP_CONSTANTS.CURRENCY_LOCALE, {}).format(
      currency
    );
  } else {
    return new Intl.NumberFormat(APP_CONSTANTS.CURRENCY_LOCALE, {
      minimumFractionDigits: 0
    }).format(currency);
  }
};
