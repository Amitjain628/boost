import APP_CONSTANTS, { API_TIMEOUTS } from '@common/constants/appConstants';
import { IApiCallerConfig } from '@utils/apiCaller';
import { getUID } from '@common/utils/maths';

export default (config: IApiCallerConfig) => {
  const {
    withBaseUrl = true,
    loader = false,
    headers,
    timeout: configTimeout,
    ...restConfig
  } = config;

  const baseURL = withBaseUrl ? APP_CONSTANTS.ESHOP_BASE_URL : '';
  let timeout = configTimeout;

  if (timeout === 0) {
    timeout =
      String(config.method).toLowerCase() === 'get'
        ? API_TIMEOUTS.GET_REQUESTS
        : API_TIMEOUTS.POST_PATCH_REQUESTS;
  }

  const uid = getUID();

  return {
    uid,
    withBaseUrl,
    loader,
    ...restConfig,
    baseURL,
    headers: {
      'Content-Type': 'application/json',
      ...headers
    },
    timeout
  };
};
