import history from '@src/client/history';
import axios, { AxiosError, AxiosResponse } from 'axios';
import store from '@store/index';
import { commonAction } from '@store/actions';
import { isBrowser } from '@utils/index';
import { IApiCallerConfig } from '@utils/apiCaller';
import axiosInterceptorConfig from '@common/utils/getAxiosRequestConfig';
import { APP_ROUTES, ERROR_CODE } from '@common/constants/appConstants';

interface ILoaderMap {
  [key: string]: null | number | NodeJS.Timer;
}
const loaderMap: ILoaderMap = {};

axios.interceptors.request.use(
  (config: IApiCallerConfig = {}): IApiCallerConfig => {
    return axiosInterceptorConfig(config);
  },
  (error: AxiosError): Promise<object> => {
    if (isBrowser && error && error.config) {
      const { loader, uid } = error.config as IApiCallerConfig;
      if (loader) {
        clearTimeout(loaderMap[uid as string] as number);
        loaderMap[uid as string] = null;
      }
    }

    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response): AxiosResponse => {
    if (isBrowser && response && response.config) {
      const { loader, uid } = response.config as IApiCallerConfig;
      if (loader) {
        clearTimeout(loaderMap[uid as string] as number);
        loaderMap[uid as string] = null;
      }
    }

    return response.data;
  },
  (error: AxiosError): Promise<object> => {
    if (isBrowser && error && error.config) {
      const { errorToast, loader, uid } = error.config as IApiCallerConfig;
      if (loader) {
        clearTimeout(loaderMap[uid as string] as number);
        loaderMap[uid as string] = null;
      }
      if (
        errorToast ||
        (error.response &&
          (error.response.status >= 400 && error.response.status <= 510))
      ) {
        if (history && error.response) {
          if (error.response.status === 404) {
            history.push(APP_ROUTES.NOT_FOUND);
          }
          if (error.response.status === 401) {
            history.push(APP_ROUTES.LOGIN);
          }
        }
        const translation = store.getState().translation.cart;

        if (errorToast) {
          if (
            error.response &&
            error.response.data &&
            error.response.data.code === ERROR_CODE.LOGIN_NOT_ACCESS
          ) {
            const errorMsg = translation.global.errors.notAccess;
            store.dispatch(commonAction.showError(errorMsg));
          } else {
            store.dispatch(
              commonAction.showError(
                (translation.global &&
                  translation.global.errors[
                    (error.response && error.response.status) || 500
                  ]) ||
                  error.message
              )
            );
          }
        }
      }
    }

    return Promise.reject(
      error.response
        ? error.response.status === 401 ||
          error.response.status === 301 ||
          error.response.status === 302
          ? error.response
          : error.response.data
        : { error: 'An unknown error has occured!' }
    );
  }
);
