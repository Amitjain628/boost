export const getServiceName = (apiEndPoint: string) => {
  const updatedEndpoint =
    apiEndPoint[0] === '/' ? apiEndPoint.replace('/', '') : apiEndPoint;

  return updatedEndpoint.indexOf('/') === -1
    ? updatedEndpoint
    : updatedEndpoint.substring(0, updatedEndpoint.indexOf('/'));
};
