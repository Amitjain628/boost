import {
  IFetchSavedStepsRequest,
  IFetchSavedStepsResponse
} from '@common/store/types';

export namespace POST {
  export type IRequest = IFetchSavedStepsRequest;
  export type IResponse = IFetchSavedStepsResponse;
}
