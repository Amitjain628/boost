import { IFetchSavedStepsResponse } from '@common/store/types';

export namespace GET {
  export type IRequest = void;
  export type IResponse = IFetchSavedStepsResponse;
}
