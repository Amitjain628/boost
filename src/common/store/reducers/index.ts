import { combineReducers } from 'redux';
import { StateType } from 'typesafe-actions';
import home from '@home/store/reducer';

import { default as translation } from './translation';
import { default as configuration } from './configuration';
import common from './common';
import { IMainState } from './types';

const rootReducer = combineReducers<IMainState>({
  translation,
  configuration,
  common,
  home
});

export type RootState = StateType<typeof rootReducer>;
export default rootReducer;
