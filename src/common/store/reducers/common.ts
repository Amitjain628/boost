import { ICommonState, ISavedStepData } from '@store/types';
import { actionConstants as CONSTANTS } from '@common/store/constants';
import { commonState } from '@common/store/states';
import withProduce from '@utils/withProduce';
import { AnyAction, Reducer } from 'redux';

const reducers = {
  [CONSTANTS.SHOW_API_ERROR]: (state: ICommonState, payload: string) => {
    state.errorToast = {
      message: payload,
      isOpen: true
    };
  },
  [CONSTANTS.REMOVE_API_ERROR]: (state: ICommonState) => {
    state.errorToast.isOpen = false;
  },
  [CONSTANTS.SET_SAVED_STEPS]: (
    state: ICommonState,
    steps: ISavedStepData[]
  ) => {
    state.savedSteps = steps;
  },
  [CONSTANTS.SET_SAVED_STEPS_FETCH_LOADING]: (
    state: ICommonState,
    loading: boolean
  ) => {
    state.loadingSavedSteps = loading;
  },
  [CONSTANTS.SET_SAVED_STEPS_FETCH_ERROR]: (
    state: ICommonState,
    error: Error | null
  ) => {
    state.loadingSavedSteps = false;
    state.savedStepsLoadingError = error;
  },
  [CONSTANTS.SET_CURRENCY_VALUE]: (state: ICommonState, currency: string) => {
    state.currency = currency;
  }
};

export default withProduce(commonState, reducers) as Reducer<
  ICommonState,
  AnyAction
>;
