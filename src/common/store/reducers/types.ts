import {
  ICommonState,
  IConfigurationState,
  ITranslationState,
} from '@common/store/types';
import { IHomeState } from '@home/store/types';

export interface IMainState {
  translation: ITranslationState;
  configuration: IConfigurationState;
  common: ICommonState;
  home: IHomeState;
}
