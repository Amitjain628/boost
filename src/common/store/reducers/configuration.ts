import { IConfigurationResponse, IConfigurationState } from '@store/types';
import { configurationConstant as CONSTANTS } from '@common/store/constants';
import { configurationInitialState } from '@common/store/states';
import APP_CONSTANTS from '@common/constants/appConstants';
import withProduce from '@utils/withProduce';
import { AnyAction, Reducer } from 'redux';
import { deepMergeObj } from '@utils/index';

const reducers = {
  [CONSTANTS.SET_CMS_CONFIGURATION_DATA]: (
    state: IConfigurationState,
    payload: IConfigurationResponse
  ) => {
    deepMergeObj(state.cms_configuration, payload);
    if (
      state &&
      state.cms_configuration &&
      state.cms_configuration.global &&
      state.cms_configuration.global.currency &&
      state.cms_configuration.global.currency.locale
    ) {
      APP_CONSTANTS.CURRENCY_LOCALE =
        state.cms_configuration.global.currency.locale;
    }
  }
};

export default withProduce(configurationInitialState, reducers) as Reducer<
  IConfigurationState,
  AnyAction
>;
