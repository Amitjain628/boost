import { ICommonState } from '@store/types';
// tslint:disable

export default (): ICommonState => ({
  errorToast: {
    message: '',
    isOpen: false
  },
  isLoginButtonClicked: false,
  currency: 'Ft',
  savedSteps: [],
  activeStep: null,
  loadingSavedSteps: true,
  savedStepsLoadingError: null
});

// SET_SAVED_STEPS_FETCH_ERROR: `${namespace}_SET_SAVED_STEPS_FETCH_ERROR`,
//   SET_SAVED_STEPS: `${namespace}_SET_SAVED_STEPS`,
//   FETCH_SAVED_STEPS_REQUESTED: `${namespace}_FETCH_SAVED_STEPS_REQUESTED`,
//   SET_SAVED_STEPS_FETCH_LOADING: `${namespace}_SET_SAVED_STEPS_FETCH_LOADING`
