import { RootState } from '@common/store/reducers';
import commonState from '@store/states/common';
import translationState from '@store/states/translation';
import configurationState from '@store/states/configuration';
import homeState from '@home/store/state';

export default (): RootState => ({
  translation: translationState(),
  configuration: configurationState(),
  common: commonState(),
  home: homeState()
});
