import { actionCreator } from '@utils/actionCreator';
import { actionConstants as CONSTANTS } from '@store/constants';
import { ISavedStepData } from '@common/store/types/common';

export default {
  showError: actionCreator<string>(CONSTANTS.SHOW_API_ERROR),
  removeError: actionCreator(CONSTANTS.REMOVE_API_ERROR),

  setSavedSteps: actionCreator<ISavedStepData[]>(CONSTANTS.SET_SAVED_STEPS),
  fetchSavedSteps: actionCreator(CONSTANTS.FETCH_SAVED_STEPS_REQUESTED),
  setSavedStepsFetchLoading: actionCreator<boolean>(
    CONSTANTS.SET_SAVED_STEPS_FETCH_LOADING
  ),
  setSavedStepsFetchError: actionCreator(CONSTANTS.SET_SAVED_STEPS_FETCH_ERROR),
  setCurrency: actionCreator<string>(CONSTANTS.SET_CURRENCY_VALUE)
};
