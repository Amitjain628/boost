export enum IMAGE_POSITION {
  FRONT = 'front',
  BACK = 'back',
  RIGHT = 'right',
  LEFT = 'left',
  BOTTOM = 'bottom',
  TOP = 'top',
  FRONT_AND_BACK = 'frontAndBack',
  OTHERS = 'others'
}

export const TARIFF_BENEFIT_TEMPLATES = {
  IMAGE: 'IMAGE',
  BUTTON: 'BUTTON',
  DROPDOWN: 'DROPDOWN',
  SIMPLE: 'SIMPLE'
};

export const TARIFF_BENEFIT_TEMPLATE_TYPE = {
  SINGLE_SELECT: 'SINGLE_SELECT',
  MULTI_SELECT: 'MULTI_SELECT'
};

export enum PAGE_THEME {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
/** USED ON PROLONGATION */
export enum IMAGE_TYPE {
  WEBP = 'webp',
  JPEG = 'jpeg',
  PNG = 'png'
}

export type pageThemeType = PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;

export enum SOCIAL_MEDIA_ICON {
  facebook = 'ec-facebook',
  twitter = 'ec-twitter',
  youtube = 'ec-youtube',
  linkedin = 'ec-linkedin',
  instagram = 'ec-instagram'
}

export enum SCREEN_RESOLUTION {
  MOBILE = 'MOBILE',
  TABLET_PORTRAIT = 'TABLET_PORTRAIT',
  TABLET_LANDSCAPE = 'TABLET_LANDSCAPE',
  DESKTOP = 'DESKTOP',
  DESKTOP_LARGE = 'DESKTOP_LARGE'
}

export enum TARIFF_LIST_TEMPLATE {
  PRICE_FIRST = 'PRICE_FIRST',
  NAME_FIRST = 'NAME_FIRST',
  DATA_FIRST = 'DATA_FIRST'
}

export enum PAYMENT_TYPE {
  CREDIT_DEBIT_CARD = 'tokenizedCard',
  PAY_BY_LINK = 'payByLink',
  PAY_ON_DELIVERY = 'payOnDelivery',
  BANK_ACCOUNT = 'bankAccount',
  MANUAL_PAYMENTS = 'manualPayments'
}

export enum STEP_DATA {
  BASKET = 'BASKET',
  PERSONAL = 'PERSONAL',
  PAYMENT = 'PAYMENT'
}

export enum STEP_PROGRESS {
  ACTIVE = 'active',
  COMPLETED = 'complete',
  INCOMPLETE = 'disabled'
}

export type stepProgressType =
  | STEP_PROGRESS.ACTIVE
  | STEP_PROGRESS.COMPLETED
  | STEP_PROGRESS.INCOMPLETE;
