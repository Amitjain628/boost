import actions from '@common/store/actions/common';
import { put, takeLatest } from 'redux-saga/effects';
import apiCaller from '@common/utils/apiCaller';
import CONSTANTS from '@common/store/constants/actionConstants';
import { apiEndpoints } from '@common/constants';

export function* fetchSavedSteps(): Generator {
  yield put(actions.setSavedStepsFetchLoading(true));
  try {
    const { url } = apiEndpoints.COMMON.FETCH_SAVED_STEPS;
    const { stepData } = yield apiCaller.get(url, {
      errorToast: true
    });
    yield put(actions.setSavedSteps(stepData));
  } catch (error) {
    yield put(actions.setSavedStepsFetchError(error));
  }
  yield put(actions.setSavedStepsFetchLoading(false));
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.FETCH_SAVED_STEPS_REQUESTED, fetchSavedSteps);
}
export default watcherSaga;
