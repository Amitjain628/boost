import { all } from 'redux-saga/effects';
import commonSaga from '@common/store/sagas/common';
import homeSaga from '@home/store/sagas';

export default function* rootSaga(): Generator {
  yield all([commonSaga(), homeSaga()]);
}
