import { put, takeLatest } from 'redux-saga/effects';
import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import CONSTANTS from '@common/constants/appConstants';
import { commonAction, configurationAction as actions } from '@store/actions';
import { logError } from '@src/common/utils';

import { IConfigurationResponse } from '../types';

export function* fetchConfiguration(): Generator {
  try {
    const baseURL = CONSTANTS.CONFIGURATION_BASE_URL;
    const response: IConfigurationResponse = yield apiCaller.get(
      `${baseURL}${apiEndpoints.CONFIG.GET_CMS_CONFIGURATION.url}`,
      { withBaseUrl: false, withCredentials: false }
    );
    const data = response;
    yield put(actions.setCMSConfiguration(data));
    try {
      if (
        data &&
        data.f2fglobal &&
        data.f2fglobal.currency &&
        data.f2fglobal.currency.currencySymbol
      ) {
        yield put(
          commonAction.setCurrency(data.f2fglobal.currency.currencySymbol)
        );
      }
    } catch (error) {
      logError(`set currecny from CMS to common state ${error}`);
    }
  } catch (error) {
    logError(error);
  }
}

export function* watchfetchConfiguration(): Generator {
  yield takeLatest('FETCH_CONFIGURATION_REQUESTED', fetchConfiguration);
}
