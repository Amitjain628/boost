import { put, takeLatest } from 'redux-saga/effects';
import apiCaller from '@common/utils/apiCaller';
import CONSTANTS from '@common/constants/appConstants';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';

import { translationAction } from '../actions';

export function* fetchTranslation(): Generator {
  try {
    const baseURL = CONSTANTS.TRANSLATION_BASE_URL;
    const response = yield apiCaller.get(
      `${baseURL}${apiEndpoints.CONFIG.GET_CMS_TRANSLATION.url}`,
      { withBaseUrl: false, withCredentials: false }
    );
    const data = response;
    yield put(translationAction.setCMSTranslation(data));
  } catch (error) {
    logError(error);
  }
}

export function* watchfetchTranslation(): Generator {
  yield takeLatest('FETCH_TRANSLATION_REQUESTED', fetchTranslation);
}
