import * as saveStepApi from '@common/types/api/common/saveStep';
import { logError } from '@src/common/utils';
import { apiEndpoints } from '@common/constants';
import apiCaller from '@common/utils/apiCaller';

export const saveActiveStep = async (
  payload: saveStepApi.POST.IRequest,
): Promise<saveStepApi.POST.IResponse | Error> => {
  const { url } = apiEndpoints.COMMON.SAVE_ACTIVE_STE;
  try {
    return await apiCaller.post(url, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};
