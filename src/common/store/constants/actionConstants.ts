const namespace = 'ESHOP';

export default {
  SET_CURRENCY_VALUE: `${namespace}_SET_CURRENCY_VALUE`,
  // Used for prolongation
  SHOW_API_ERROR: `${namespace}_SHOW_API_ERROR`,
  REMOVE_API_ERROR: `${namespace}_REMOVE_API_ERROR`,

  SHOW_LOADER: `${namespace}_SHOW_LOADER`,
  REMOVE_LOADER: `${namespace}_REMOVE_LOADER`,
  IS_USER_LOGGED_IN: `${namespace}_IS_USER_LOGGED_IN`,
  // Use for genric API error. used in eshop Web
  SHOW_GENRIC_ERROR: `${namespace}_SHOW_GENRIC_ERROR`,
  REMOVE_GENRIC_ERROR: `${namespace}_REMOVE_GENRIC_ERROR`,
  // fetch stepsconstants
  SET_SAVED_STEPS_FETCH_ERROR: `${namespace}_SET_SAVED_STEPS_FETCH_ERROR`,
  SET_SAVED_STEPS: `${namespace}_SET_SAVED_STEPS`,
  FETCH_SAVED_STEPS_REQUESTED: `${namespace}_FETCH_SAVED_STEPS_REQUESTED`,
  SET_SAVED_STEPS_FETCH_LOADING: `${namespace}_SET_SAVED_STEPS_FETCH_LOADING`
};
