import {
  PAGE_THEME,
  PAYMENT_TYPE,
  TARIFF_LIST_TEMPLATE
} from '@common/store/enums/index';

export type operatingSystem = 'Windows' | 'OSX' | 'Android';

export interface IBasketConfiguration {
  continueShoppingUrl: string;
  summarySnapshotFrequency: number;
}

export interface IConfigurationResponse {
  global: {
    stickyOrderSummary: IStickyOrderSummary;
  };
  f2fglobal: IF2FGlobalConfiguration;
  modules: {
    basket: IBasket;
    productList: IProductListConfiguration;
    checkout: ICheckout;
    productDetailed: IProductDetailedConfiguration;
  };
}

export interface ITariffConfiguration {
  tariffListingTemplate:
    | TARIFF_LIST_TEMPLATE.NAME_FIRST
    | TARIFF_LIST_TEMPLATE.DATA_FIRST
    | TARIFF_LIST_TEMPLATE.PRICE_FIRST;
  defaultTariffBenefitTemplate: string;
  tariffBenefitTemplates: {
    freeApp: ITariffBenefitTemplatesItem;
    extraData: ITariffBenefitTemplatesItem;
  };
}

export interface ITariffBenefitTemplatesItem {
  templateName: string;
  templateType: string;
}
export interface IProductDetailedConfiguration {
  showBasketDrawer: boolean;
  storage: string;
  color: string;
  numberOfInstalments: string;
  theme: PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;
  newProductBeforeLaunchDays: number;
  showDisabledAttributes: boolean;
  disabledAttributesClickable: boolean;
  buyDeviceWithInstallments: boolean;
  productDetailedTemplate1: {
    showOnlineStockAvailability: boolean;
    showVariantSelection: boolean;
    showDevicePrice: boolean;
    showTariffInfo: boolean;
    showHighlightPromo: boolean;
    showUnlockDiscount: boolean;
    showTodayPrice: boolean;
    showbadges: boolean;
    showQuantity: boolean;
    showAddToCompare: boolean;
    showNotifyMe: boolean;
    showOverViewSection: boolean;
    showTechnicalSpecification: boolean;
    showUpSellSection: boolean;
    showEnableZoomEffect: boolean;
  };
}

export interface IGoogleMapConfiguration {
  apiKey: string;
  rangeCover: number;
  zoom: number;
  center: IMapCenter;
}

interface IMapCenter {
  lat: number;
  lng: number;
}

export interface ILoginFormRules {
  password: IPassword;
  phoneNumber: IPhoneNumber;
  userName: IPhoneNumber;
  email: IEmail;
}

export interface IEmail {
  regex?: string;
}

export interface IPhoneNumber {
  regex?: string;
  minLength: number;
  maxLength: number;
}

export interface IPassword {
  regex?: string;
  minLength: number;
  minNumberOfDigits: number;
  minLowerCaseLetters: number;
  minUpperCaseLetters: number;
}
export interface ILoginRegistrationMethods {
  emailOTP: boolean;
  MSISDNOTP: boolean;
  usernamePassword: boolean;
  automaticMSISDN: boolean;
  socialAccount: boolean;
  googleAccount: boolean;
  QRCodeScan: boolean;
}

export interface IResendOTPWaitingPeriod {
  mail: number;
  sms: number;
}

export interface ISEO {
  title: string;
  description: string;
}

export interface ICheckoutSEO {
  personalInfo: ISEO;
  shipping: ISEO;
  payment: ISEO;
  billing: ISEO;
  orderReview: ISEO;
}
export interface ILogin {
  social: ISocial;
}
export interface ISocial {
  google: IGoogle;
}

export interface IGoogle {
  client: {
    id: string;
    scope: string;
    cookiePolicy: string;
    script: string;
  };
}

export interface ISearchConfiguration {
  searchDebounce: number;
  isFilterSticky: boolean;
}

export interface IGlobalConfiguration {
  braintreeAuthorization: string;
  seo: {
    checkout: ICheckoutSEO;
  };
  search: ISearchConfiguration;
  jsonLd: IJsonLd;
  googleMap: IGoogleMapConfiguration;
  termsAndConditionsUrl: string;
  currency: ICurrencyConfiguration;
  orderTrackingUrl: string;
  loginWithEmailConfirmation: boolean;
  shouldTermsAndConditionsOpenInNewTab: boolean;
  stickyOrderSummary: IStickyOrderSummary;
  loginFormRules: ILoginFormRules;
  loginRegistrationMethods: ILoginRegistrationMethods;
  resendOTPWaitingPeriod: IResendOTPWaitingPeriod;
  authentication: IAuthentication;
  paymentMethods: IPaymentMethods;
  footer: IFooter;
  header: IHeader;
  creditCheckRequired: boolean;
  scriptForGTA: string;
  noScriptForGTA: string;
  mqtHost: string;
  mqtPort: number;
  mqtReconnectTimeout: number;
  mqtUserName: string;
  mqtPassword: string;
  numberOfRetries: number;
  eventTopicName: string;
}

export interface IJsonLd {
  corporateContactAndLogo: ICorporateContactAndLogo & IShow;
  webPage: IWebPage & IShow;
  sitelinksSearchbox: ISitelinksSearchbox & IShow;
  androidApplication: IMobileApplication & IShow;
  iosApplication: IMobileApplication & IShow;
  webApplication: IWebApplication & IShow;
  socialProfile: ISocialProfile & IShow;
  sameAs: {
    facebook: string;
    instagram: string;
    linkedin: string;
    youtube: string;
    pinterest: string;
    wikipedia: string;
    twitter: string;
  };
}

export interface IConfigurationState {
  cms_configuration: {
    global: IGlobalConfiguration;
    f2fglobal: IF2FGlobalConfiguration;
    modules: {
      login: ILogin;
      basket: IBasket;
      productList: IProductListConfiguration;
      checkout: ICheckout;
      category: ICategory;
      productDetailed: IProductDetailedConfiguration;
      tariff: ITariffConfiguration;
      prolongation: IProlongationConfig;
      f2f: IF2FConfiguration;
    };
  };
}

export interface IF2FGlobalConfiguration {
  currency: ICurrencyConfiguration;
}
export interface IF2FConfiguration {
  password: IPassword;
  username: IUserName;
  termsAndConditionsNewTab: boolean;
  termsAndConditionsUrl: string;
}

export interface IUserName {
  regex?: string;
}

export interface IPassword {
  regex?: string;
  minLength: number;
  minNumberOfDigits: number;
  minLowerCaseLetters: number;
  minUpperCaseLetters: number;
}

/** USED ON PROLONGATION */
export interface IProlongationConfig {
  form: {
    email: {
      validation: string;
      validationMessage: string;
    };
    deliveryContact: {
      validation: string;
      validationMessage: string;
      maxLength: number;
    };
    deliveryAddress: {
      maxLength: number;
      validation: string;
      validationMessage: string;
    };
  };
  gaCode: string;
  publicKey: string;
  landingPageTitle: string;
  personalDetailsPageTitle: string;
  orderPageTitle: string;
  webviewCloseTime: number;
}

export interface IDtLogo {
  isMobileLogoDifferent: boolean;
  desktop: ILogoDetails;
  mobile: ILogoDetails;
  heightWidthUnit: string;
}

export interface ILogoDetails {
  url: string;
  imageAltText: string;
  aspectRatio: number;
  height: string;
  width: string;
}
export interface IHeader {
  dtLogo: IDtLogo;
  privateLink: string;
  businessLink: string;
  touristLink: string;
  specialOfferHtml: string;
  headertype: number;
  headerColumns: number;
}
export interface IFooter {
  customerCareNumber: number;
  storeLocatorLink: string;
  newsletterLink: string;
  socialMedia: ISocialMedia;
  footerStaticLinks: IFooterStaticLinks;
}

export interface IFooterStaticLinks {
  aboutUs: string;
  contact: string;
  privacyPolicy: string;
  cookies: string;
  support: string;
  secure: string;
}
export interface ISocialMedia {
  facebook: ISocialMediaFooterLink;
  twitter: ISocialMediaFooterLink;
  linkedin: ISocialMediaFooterLink;
  instagram: ISocialMediaFooterLink;
  youtube: ISocialMediaFooterLink;
}

export interface ISocialMediaFooterLink {
  key: string;
  show: boolean;
  link: string;
}
export interface ICurrencyConfiguration {
  currencySymbol: string;
  isPrecede: boolean;
  locale: string;
}

export interface ICategory {
  bestDeviceOffersCategoryId: null | string;
  backgroundImage: string;
  theme: PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;
}

export interface IPaymentMethods {
  upfront: IUpfrontPayment;
  monthly: IMonthlyPayment;
  availablePaymentMode: IAavailablePaymentMode;
}
export interface IAavailablePaymentMode {
  maestro: IShowHide;
  applePay: IShowHide;
  googlePay: IShowHide;
  visa: IShowHide;
  payPal: IShowHide;
  worldPay: IShowHide;
}

export interface IPaymentMethodResponseTypes {
  show?: boolean;
  link?: string;
  labelKey: string;
}

export interface IUpfrontPayment {
  tokenizedCard: IPaymentMethodResponseTypes;
  payOnDelivery: IPaymentMethodResponseTypes;
  payByLink: IPaymentMethodResponseTypes;
  bankAccount: IPaymentMethodResponseTypes;
  manualPayments: IPaymentMethodResponseTypes;
  defaultPaymentMethod: PAYMENT_TYPE;
}

export interface IMonthlyPayment {
  tokenizedCard: IPaymentMethodResponseTypes;
  payOnDelivery: IPaymentMethodResponseTypes;
  payByLink: IPaymentMethodResponseTypes;
  bankAccount: IPaymentMethodResponseTypes;
  manualPayments: IPaymentMethodResponseTypes;
  defaultPaymentMethod: PAYMENT_TYPE;
}

export interface IShowHide {
  show: boolean;
}

export interface IAuthentication {
  encryption: IEncryption;
  apiLatency: number;
  masking: IMasking;
}

export interface IMasking {
  email: string;
  phone: string;
}
export interface IEncryption {
  publicKey: string;
}

interface IBasket {
  continueShoppingUrl: string;
  summarySnapshotFrequency: number;
}

export interface IProductListConfiguration {
  sortBy: {
    lowestPrice: {
      show: boolean;
      labelKey: string;
      isDefault: boolean;
    };
    mostPopular: {
      show: boolean;
      labelKey: string;
      isDefault: boolean;
    };
    newlyAdded: {
      show: boolean;
      labelKey: string;
      isDefault: boolean;
    };
    discountedDevices: {
      show: boolean;
      labelKey: string;
      isDefault: boolean;
    };
    specialOffer: {
      show: boolean;
      labelKey: string;
      isDefault: boolean;
    };
  };
  itemPerPage: number;
  itemPerPageList: string;
  sortOutStock: boolean;
  paginationType: string;
  installment: number;
  backgroundImage: string;
  viewAllLink: string;
  theme: PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;
  tariffName: string;
}

interface IStickyOrderSummary {
  mobile: boolean;
  desktop: boolean;
  summarySnapshotFrequency: number;
}

export interface ICheckout {
  form: IForm;
  shipping: IShipping;
  billingInfo: IBillingConfiguration;
  orderNotification: { sms: boolean };
  orderReview: IOrderReview;
  mnp: IMNPConfiguration;
  mns: IMNSConfiguration;
  identityVerification: {
    enabled: boolean;
  };
  searchSize: number;
}

export interface IOrderReview {
  termsAndConditionsUrl: string;
  shouldTermsAndConditionsOpenInNewTab: boolean;
  enablePaymentProcess: boolean;
}

export interface IShipping {
  form: IForm;
  shippingType: IShippingType;
}

export interface IShippingType {
  deliverToAddress: {
    show: boolean;
    deliveryOptions: IDeliveryOptions;
  };
  pickUpAtStore: IShow;
  parcelLocker: IShow;
  pickUpPoints: IShow;
}

export interface IDeliveryOptions {
  standard: IShow;
  withinTime: IShow;
  pickADate: IShow;
}

export interface IShow {
  show: boolean;
}

export interface IBillingConfiguration {
  form: IForm;
  billingType: IBillingType;
  eBillDiscount: boolean;
}

export interface IBillingType {
  sameAsShippingInfo: IShow;
  sameAsPersonalInfo: IShow;
  differentAddress: IShow;
  eBill: IDefault;
  paperBill: IDefault;
}

export interface IShow {
  show: boolean;
}
export interface IDefault {
  default: boolean;
}

export interface IForm {
  fields: IFields;
}
export interface IFields {
  [key: string]: IFieldAttribiute;
}
interface IFieldAttribiute {
  order: number;
  labelKey: string;
  inputType: string;
  show: boolean;
  readOnly: boolean;
  mandatory: boolean;
  hidden?: true;
  validation: IFieldValdation;
  creditCheckField?: boolean;
}

export type validationType =
  | 'max'
  | 'min'
  | 'regex'
  | 'between'
  | 'none'
  | 'min-uppercase'
  | 'min-lowercase'
  | 'min-digit'
  | '';

export interface IFieldValdation {
  type: validationType;
  value: string;
  message: string;
  // tslint:disable-next-line:max-file-line-count
}

export interface IServiceStatus {
  show: boolean;
  migrationOptions: IMigrationOption;
  labelKey: string;
}
export interface IMigrationOption {
  desiredDate: IDesiredDate;
  endOfPromotion: IMigrationType;
  endOfAgreement: IMigrationType;
}
export interface IDesiredDate {
  show: boolean;
  labelKey: string;
  value: number;
}
export interface IMigrationType {
  show: boolean;
  labelKey: string;
}

export interface IService {
  hybrid: IServiceStatus;
  prepaid: IServiceStatus;
  postpaid: IServiceStatus;
}

export interface IMNPConfiguration {
  categoryId: string;
  serviceStatusType: IService;
  show: boolean;
  labelKey: string;
  verificationMethods: {
    MSISDNOTP: boolean;
  };
}

export interface IMNSConfiguration {
  categoryId: string;
  show: boolean;
  labelKey: string;
}

export interface IWebApplication {
  name: string;
  operatingSystem: operatingSystem;
  applicationUrl: string;
  aggregateRating: {
    ratingValue: string;
    ratingCount: string;
  };
  softwareVersion: string;
  releaseNotes: string;
}
export interface IMobileApplication {
  name: string;
  operatingSystem: string;
  applicationUrl: string;
  downloadUrl: string;
  aggregateRating: {
    ratingValue: string;
    ratingCount: string;
  };
  softwareVersion: string;
  releaseNotes: string;
}

export interface ICorporateContactAndLogo {
  telephone: string;
  contactType: string;
  logo: string;
}

export interface IWebPage {
  id: string;
  inLanguage: string;
  name: string;
  isPartOf: string;
  about: string;
  datePublished: string;
  dateModified: string;
  description: string;
}

export interface ISocialProfile {
  id: string;
  name: string;
  description: string;
  caption: string;
}
export interface IUrl {
  url: string;
}

export interface ISitelinksSearchbox {
  id: string;
  name: string;
  publisherId: string;
  // tslint:disable-next-line: max-file-line-count
}
