import { STEP_DATA } from '@common/store/enums';

export interface ICommonState {
  errorToast: {
    message: string;
    isOpen: boolean;
  };
  currency: string;
  isLoginButtonClicked: boolean;
  savedSteps: ISavedStepData[];
  activeStep: string | null;
  loadingSavedSteps: boolean;
  savedStepsLoadingError: Error | null;
  closeToast?(): void;
}

export type savedDataStepType =
  | STEP_DATA.BASKET
  | STEP_DATA.PERSONAL
  | STEP_DATA.PAYMENT;
export interface ISavedStepData {
  step: savedDataStepType;
  data: object;
}
export interface IFetchSavedStepsRequest {
  stepData: ISavedStepData[];
}
export interface IFetchSavedStepsResponse {
  stepData: ISavedStepData[];
}

export interface ILoading {
  errorToast: { isOpen: boolean; message: string };
  isLoggedIn: boolean;
  isLoginButtonClicked: boolean;
  loading: boolean;
  routeFromBasketToLogin: boolean;
}
