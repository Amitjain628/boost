export default {
  HOME: '/',
  ERROR: '/error',
  404: '/not-found'
};
