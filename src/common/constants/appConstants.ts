export default {
  CONFIGURATION_BASE_URL: '',
  CONFIGURATION_API_KEY: '',
  TRANSLATION_BASE_URL: '',
  ESHOP_BASE_URL: '',
  LANGUAGE_CODE: '',
  COUNTRY_CODE: '',
  CONTENT_LANGUAGE: '',
  LOADER_MIN_DURATION: 300,
  CHANNEL: '',
  RUM_SERVER_URL: '',
  RUM_SERVER_NAME: '',
  RUM_VERSION: '',
  // tslint:disable-next-line:max-line-length
  PUBLIC_KEY: `-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4kmzj3sOxZ0i9KfUECP9kMjY3ejZWRPDHfgrgLF8Ih261xm+d1cySEJ01qdO6/HnIYIqE1sDYEs/xzprSbcO/amdbHz4/iSSdtyaVbQfEmmdiAabYE3E7g8jW0MRTGo+9CmE0M4sUxDVyeW1lkD+Ojiboi7xrscr5GcA/4e8NSAQbOu1UXXNunzWNu1AWZ5nQGux8sfWN8UkARn03/xNY2hK8VlwrZo5pdc26JHfiDRvka00VIs5DuWJc0oi/fDcVN1A6eD1oszKlhFZV5olTz8c+00rlvbD2QCv+NtdfkXCRg831E66nrduqUZbRS1iUpl8AFXCCK9dQ82D7Pf40wIDAQAB-----END PUBLIC KEY-----`,
  OPTIONAL: 'optionalText',
  NUMBER_OF_OTP_INPUTS: 4,
  LOGIN: 'login',
  USERNAME: 'username',
  EMAIL: 'email',
  PHONE_NUMBER: 'phoneNumber',
  IS_USER_LOGGED_IN: 'IsLoggedIn',
  USER_ID: 'userId',
  DEVICE_ID: 'c17416c7-39bc-418d-acde-ea3e0fb5cd47',
  DEVICE_MODAL: 'Samsung 910N',
  PUSH_TOKEN: 'f16PCS40qN',
  OS: 'android',
  COUNTRY: 'header-country',
  LANGUAGE: 'language',
  FOOTER: 'footer',
  S3_IMAGE_URL:
    'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius',
  CURRENCY_LOCALE: 'pl'
};

export const REGEX = {
  EXTRACT_BASE_URL: /(http(s)?:\/\/)|(\/.*){1}/g,
  VALIDATE_URL: /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g,
  EMAIL: /^\w+([.-]\w+)*@\w+([.-]\w+)*(\.\w{2,4})+$/,
  PHONE: /^(\(?\+?[0-9]*\)?)?[0-9\- ]*$/
};

export const VALIDATIONS = {
  email: REGEX.EMAIL,
  phone: REGEX.PHONE,
  url: REGEX.VALIDATE_URL
};

export const HISTORY_STATES = {
  POP: 'POP',
  PUSH: 'PUSH'
};

export const DATE_FORMATTING = 'MM/DD/YYYY';

export enum ERROR_CODE_BFF {
  LEAD_ALREADY_EXIST = 'leadAlreadyExist',
  DT_UNKNOWN_ERROR = 'dtUnknownError',
  MANDATORY_ATTRIBUTE_NULL_OR_EMPTY = 'mandatoryMissing',
  VARIANTS_NOT_FOUND = 'variantsNotFound',
  HANDLE_401 = 'handle401'
}

export enum API_TIMEOUTS {
  GET_REQUESTS = 5000,
  POST_PATCH_REQUESTS = 10000,
  CHECKOUT_REQUESTS = 20000
}

export enum VALIDATION_TYPE {
  MIN = 'min',
  MAX = 'max',
  REGEX = 'regex',
  BETWEEN = 'between',
  NONE = 'none',
  MIN_UPPERCASE = 'min-uppercase',
  MIN_LOWERCASE = 'min-lowercase',
  MIN_DIGIT = 'min-digit'
}

export enum APP_ROUTES {
  LOGIN = '/login',
  F2F = '/home',
  NOT_FOUND = '/not-found'
}

export enum ERROR_CODE {
  LOGIN_NOT_ACCESS = 'F2F_INVALID_USER_CODE'
}
