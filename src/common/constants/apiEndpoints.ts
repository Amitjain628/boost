// tslint:disable

export default {
  CONFIG: {
    GET_CMS_CONFIGURATION: {
      url: "/latest/config.json",
      method: "GET",
      params: [],
    },
    GET_CMS_TRANSLATION: {
      url: "/web-strings.json",
      method: "GET",
      params: [],
    },
    GET_MEGA_MENU: {
      url: (cmsConfigKey: string, language: string) =>
        `hns/config/all/${cmsConfigKey}?language=${language}`,
      method: "GET",
      params: [],
    },
  },
  HOME: {
    GET_USER: {
      url: (search: string) =>
        `/search/users?page=1&query=${search}&client_id=517be3fec41e9cb52896bc25c7a688d8aa02419405f7f37dfb2670fcde9ee45f`,
      method: "GET",
      params: [],
    },
    GET_USER_PHOTOS: {
      url: (userName: string) =>
        `/users/${userName}/photos?client_id=517be3fec41e9cb52896bc25c7a688d8aa02419405f7f37dfb2670fcde9ee45f`,
      method: "GET",
      params: [],
    },
  },
  COMMON: {
    SAVE_ACTIVE_STE: {
      url: "/journey/v1/f2fstep",
      method: "POST",
      params: [],
    },
    FETCH_SAVED_STEPS: {
      url: "/journey/v1/f2fstep",
      method: "GET",
      params: [],
    },
  },
};
