import React, { ReactNode } from 'react';
import { connect } from 'react-redux';
import actions from '@common/store/actions/common';
import { Dispatch } from 'redux';
import { RootState } from '@store/reducers';

interface IProps {
  message: string;
  isOpen: boolean;
  closeToast(): void;
}

class Index extends React.Component<IProps> {
  render(): ReactNode {

    return (
    <></>
    );
  }
}

export const mapStateToProps = (state: RootState) => {
  return {
    message: state.common.errorToast.message,
    isOpen: state.common.errorToast.isOpen
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  closeToast(): void {
    dispatch(actions.removeError());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index);
