import React from 'react';
import { mount, shallow } from 'enzyme';
import { BrowserRouter as Router, StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { RootState } from '@common/store/reducers';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'dt-components';

import NotFound from '.';

describe('<NotFound />', () => {
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  test('should render properly', () => {
    const component = shallow(
      <Router>
        <NotFound />
      </Router>
    );
    expect(component).toMatchSnapshot();
  });

  test('renders without crashing', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <NotFound />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
