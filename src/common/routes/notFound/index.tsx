import React, { ReactNode } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export interface IProps {
  title?: string | null;
}

export const StyledNotFoundPage = styled.div`
  z-index: 999999;
`;

class NotFound extends React.Component<IProps> {
  static propTypes = {
    title: PropTypes.string
  };

  render(): ReactNode {
    return <StyledNotFoundPage>404</StyledNotFoundPage>;
  }
}

export default NotFound;
