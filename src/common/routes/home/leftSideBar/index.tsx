import React from 'react';
import Users from '@home/Users';

import { IUserDetails } from '../store/types';

export interface IProps {
  userDetails: IUserDetails[];
  searchUser(search: string): void;
  setIntialState(): void;
  getUserPhotos(userName: string): void;
}

export interface IState {
  value: string;
}

export class LeftSideBar extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      value: '',
    };
    this.searchUserName = this.searchUserName.bind(this);
  }

  // tslint:disable-next-line:no-any
  searchUserName(event: any): void {
    this.setState({
      value: event.target.value,
    });
  }

  render(): React.ReactNode {
    const { userDetails, getUserPhotos } = this.props;

    return (
      <div className='left-container'>
        <div className='search-container'>
          <form>
            <input
              type='text'
              value={this.state.value}
              onChange={this.searchUserName}
              className='textbox'
              placeholder='Search'
            />
            <button
              type='button'
              onClick={() => {
                if (this.state.value && this.state.value.length) {
                  this.props.searchUser(this.state.value);
                } else {
                  this.props.setIntialState();
                }
              }}
              className='button'
            />
          </form>
        </div>
        {userDetails && userDetails.length
          ? userDetails.map((userDetail, index) => (
              <Users
                key={index}
                getUserPhotos={getUserPhotos}
                username={userDetail.username}
                firstName={userDetail.firstName}
                location={userDetail.location}
                profileImage={userDetail.profile_image}
              />
            ))
          : null}
        {userDetails && userDetails.length === 0 ? (
          <div>No user present</div>
        ) : null}
      </div>
    );
  }
}

export default LeftSideBar;
