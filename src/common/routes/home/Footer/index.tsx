import React from 'react';

export class Footer extends React.Component<{}> {
  render(): React.ReactNode {
    return (
      <section className='lab_social_icon_footer'>
        <div className='container'>
          <div className='text-center center-block' />
        </div>
      </section>
    );
  }
}

export default Footer;
