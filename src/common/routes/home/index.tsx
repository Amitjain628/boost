import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import Footer from '@home/Footer';
import LeftSideBar from '@home/LeftSideBar';
import PhotoSection from '@home/PhotoSection';
import { connect } from 'react-redux';
import { RootState } from '@common/store/reducers';

import { StyledGrid } from './styles';
import { mapDispatchToProps, mapStateToProps } from './mapProps';
import { IMapDispatchToProps, IMapStateToProps } from './types';

export type IProps = IMapStateToProps &
  IMapDispatchToProps &
  RouteComponentProps;

export class Home extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  render(): React.ReactNode {
    const {
      searchUserName,
      userDetails,
      getUserPhotos,
      userPhotos,
      setIntialState,
    } = this.props;

    return (
      <>
        <StyledGrid>
          <LeftSideBar
            userDetails={userDetails}
            searchUser={searchUserName}
            setIntialState={setIntialState}
            getUserPhotos={getUserPhotos}
          />
          <PhotoSection userPhotos={userPhotos} />
        </StyledGrid>
        <Footer />
      </>
    );
  }
}

export default withRouter(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps,
  )(Home),
);
