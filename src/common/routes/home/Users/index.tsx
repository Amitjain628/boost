import React from 'react';

import { IProfileImage } from '../store/types';

export interface IProps {
  firstName: string | null;
  location: string | null;
  profileImage: IProfileImage;
  username: string;
  getUserPhotos(userName: string): void;
}

export class Users extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  render(): React.ReactNode {
    const {
      firstName,
      location,
      profileImage,
      getUserPhotos,
      username,
    } = this.props;

    return (
      <div className='cursor' onClick={() => getUserPhotos(username)}>
        <span>{firstName}</span>
        <span>
          <img src={profileImage.small} />
        </span>
        <span>{location}</span>
      </div>
    );
  }
}

export default Users;
