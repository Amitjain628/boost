import { IUserDetails, IUserPhotos } from '@home/store/types';

export interface IMapDispatchToProps {
  searchUserName(userName: string | null): void;
  getUserPhotos(userName: string): void;
  setIntialState(): void;
}

export interface IMapStateToProps {
  userDetails: IUserDetails[];
  userPhotos: IUserPhotos[];
}
