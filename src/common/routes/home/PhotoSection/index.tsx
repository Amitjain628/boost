import React from 'react';
import Photos from '@home/Photos';

import { IUserPhotos } from '../store/types';

export interface IProps {
  userPhotos: IUserPhotos[];
}

export class PhotoSection extends React.Component<IProps> {
  render(): React.ReactNode {
    const { userPhotos } = this.props;

    return (
      <div className='right-container'>
        {userPhotos && userPhotos.length
          ? userPhotos.map(photos => (
              <Photos key={photos.id} image={photos.urls.regular} />
            ))
          : null}
        {userPhotos.length === 0 ? <span>No photos present</span> : null}
      </div>
    );
  }
}

export default PhotoSection;
