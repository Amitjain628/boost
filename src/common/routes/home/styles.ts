import styled from 'styled-components';

export const StyledGrid = styled.div`
  display: flex;
  height: 75%;
  max-height: 75%;
  border: 1px solid black;
  .left-container {
    flex: 0 1 30%;
    min-height: 100%;
    max-width: 30%;
    border-right: 1px solid black;
    padding: 0.5em;
  }
  .right-container {
    flex: 0 1 70%;
    display: flex;
    height: 100%;
    max-width: 70%;
    flex-wrap: wrap;
    align-items: flex-start;
  }

  .searchTerm:focus {
    color: #00b4cc;
  }

  .searchButton {
    width: 40px;
    height: 36px;
    border: 1px solid #00b4cc;
    background: #00b4cc;
    text-align: center;
    color: #fff;
    border-radius: 0 5px 5px 0;
    cursor: pointer;
    font-size: 20px;
  }

  .search-container {
    height: 70px;
  }

  .cursor {
    cursor: pointer;
    padding: 12px;
    border: 1px solid black;
  }

  form {
    outline: 0;
    float: left;
    -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12),
      0 1px 2px rgba(0, 0, 0, 0.24);
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    -webkit-border-radius: 4px;
    border-radius: 4px;
  }

  form > .textbox {
    outline: 0;
    height: 42px;
    width: 244px;
    line-height: 42px;
    padding: 0 16px;
    background-color: rgba(255, 255, 255, 0.8);
    color: #212121;
    border: 0;
    float: left;
    -webkit-border-radius: 4px 0 0 4px;
    border-radius: 4px 0 0 4px;
  }

  form > .textbox:focus {
    outline: 0;
    background-color: #fff;
  }

  form > .button {
    outline: 0;
    background: none;
    background-color: rgba(38, 50, 56, 0.8);
    float: left;
    height: 42px;
    width: 42px;
    text-align: center;
    line-height: 42px;
    border: 0;
    color: #fff;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: 16px;
    text-rendering: auto;
    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
    -webkit-transition: background-color 0.4s ease;
    transition: background-color 0.4s ease;
    -webkit-border-radius: 0 4px 4px 0;
    border-radius: 0 4px 4px 0;
  }

  form > .button:hover {
    background-color: rgba(0, 150, 136, 0.8);
  }

  .lab_social_icon_footer {
    padding: 40px 0;
    background-color: #dedede;
  }

  .lab_social_icon_footer a {
    color: #333;
  }

  .lab_social_icon_footer .social:hover {
    -webkit-transform: scale(1.1);
    -moz-transform: scale(1.1);
    -o-transform: scale(1.1);
  }

  .lab_social_icon_footer .social {
    -webkit-transform: scale(0.8);
    /* Browser Variations: */

    -moz-transform: scale(0.8);
    -o-transform: scale(0.8);
    -webkit-transition-duration: 0.5s;
    -moz-transition-duration: 0.5s;
    -o-transition-duration: 0.5s;
  }
  /*
    Multicoloured Hover Variations
*/

  .lab_social_icon_footer .social-fb:hover {
    color: #3b5998;
  }

  .lab_social_icon_footer .social-tw:hover {
    color: #4099ff;
  }

  .lab_social_icon_footer .social-gp:hover {
    color: #d34836;
  }

  .lab_social_icon_footer .social-em:hover {
    color: #f39c12;
  }
`;
