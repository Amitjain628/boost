import actions from '@home/store/actions';
import { Dispatch } from 'redux';
import { RootState } from '@common/store/reducers';

import { IMapDispatchToProps, IMapStateToProps } from './types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  userDetails: state.home.userDetails,
  userPhotos: state.home.userPhotos,
});

export const mapDispatchToProps = (
  dispatch: Dispatch,
): IMapDispatchToProps => ({
  searchUserName(search: string): void {
    dispatch(actions.setUsername(search));
  },
  getUserPhotos(userName: string): void {
    dispatch(actions.getUserPhotos(userName));
  },
  setIntialState(): void {
    dispatch(actions.setIntialState());
  },
});
