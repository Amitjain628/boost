import { IResponse } from '@home/store/types';
import { apiEndpoints } from '@common/constants';
import apiCaller from '@common/utils/apiCaller';

export const fetchUserDetailService = async (
  payload: string,
): Promise<IResponse> => {
  const { url } = apiEndpoints.HOME.GET_USER;
  try {
    return await apiCaller.get(url(payload));
    // tslint:disable-next-line:no-useless-catch
  } catch (e) {
    throw e;
  }
};

export const fetchUserPhotos = async (payload: string): Promise<IResponse> => {
  const { url } = apiEndpoints.HOME.GET_USER_PHOTOS;
  try {
    return await apiCaller.get(url(payload));
    // tslint:disable-next-line:no-useless-catch
  } catch (e) {
    throw e;
  }
};
