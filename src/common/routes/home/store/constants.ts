const namespace = 'HOME';

export default {
  SET_LOADING: `${namespace}_SET_LOADING`,
  SET_ERROR: `${namespace}_SET_ERROR`,
  SET_USERDETAILS: `${namespace}_SET_USERDETAILS`,
  FETCH_USERDETAILS: `${namespace}_FETCH_USERDETAILS`,
  GET_USER_PHOTOS: `${namespace}_GET_USER_PHOTOS`,
  SET_USER_PHOTOS: `${namespace}_SET_USER_PHOTOS`,
  SEARCH_USERNAME: `${namespace}_SEARCH_USERNAME`,
  SET_INITAL_STATE: `${namespace}_SET_INITAL_STATE`,
};
