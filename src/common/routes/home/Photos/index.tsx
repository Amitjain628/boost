import React from 'react';

export interface IProps {
  image: string;
}

export class Photos extends React.Component<IProps> {
  render(): React.ReactNode {
    const { image } = this.props;

    return (
      <div>
        <img src={image} />
      </div>
    );
  }
}

export default Photos;
