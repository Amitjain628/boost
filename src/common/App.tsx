import { Helmet } from 'react-helmet';
import GlobalStyle from '@common/reset';
import Toast from '@common/Toast';
import styled from 'styled-components';
import PreLoader from '@common/Loader';
import React from 'react';
import CommonGlobalStyle from '@common/common';
import { Redirect, Route, RouteComponentProps, Switch } from 'react-router';
import routes from '@common/routes';
import APP_CONSTANTS, { APP_ROUTES } from '@common/constants/appConstants';
import { isBrowser } from '@src/common/utils';
import store from '@store/index';

import { breakpoints, colors } from './variables';

export interface IProps {
  exact?: boolean;
  path?: string;
  // tslint:disable-next-line:no-any
  component?: React.ComponentType<RouteComponentProps<any>>;
}

const StyledAppInner = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  justify-content: space-between;


  .proceedAsGuestModal {
    .overlay {
      overflow: auto;
    }
    .overlay,
    .outsideClick,
    .contentWrap,
    .StyledLoginFlowWithProceedAsGuest {
      height: 100%;
      width: 100%;
    }
  }

  .progressModalWrap {
    .overlay {
      /* background: ${colors.black}; */
    }
  }

  .progressModal {
    .overlay {
      align-items: flex-end;

      .outsideClick,
      .contentWrap {
        width: 100%;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .progressModal {
      .overlay {
        align-items: center;
        .outsideClick,
        .contentWrap {
          width: 57rem;
        }
      }
    }
  }
`;

export const checkUserLogin = () => {
  return (
    isBrowser &&
    localStorage.getItem(APP_CONSTANTS.IS_USER_LOGGED_IN) !== null &&
    localStorage.getItem(APP_CONSTANTS.IS_USER_LOGGED_IN) === 'true'
  );
};

const PrivateRoute = ({ component: Component, ...rest }: IProps) => (
  <Route
    {...rest}
    render={props =>
      checkUserLogin() ? (
        Component ? (
          <Component {...props} />
        ) : null
      ) : (
        <Redirect
          to={{
            pathname: '/',
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export default () => {
  const titleTranslation = store
    ? store.getState().translation.f2f.global.title
    : '';

  return (
    <>
      <StyledAppInner className='styledAppInner'>
        <Helmet>
          <title>{titleTranslation ? titleTranslation : ''}</title>
          <meta
            name='description'
            content='react typescript ssr with code split'
          />
        </Helmet>
        <Toast />
        <PreLoader />
        <Switch>
          {routes.map((route, index) => {
            return route.isProtected ? (
              <PrivateRoute
                key={index}
                path={route.path}
                component={route.component}
              />
            ) : (
              <Route
                path={route.path}
                key={index}
                exact
                render={props =>
                  route.path === APP_ROUTES.LOGIN && checkUserLogin() ? (
                    <Redirect
                      to={{
                        pathname: '/'
                      }}
                    />
                  ) : route.component ? (
                    <route.component {...props} />
                  ) : null
                }
              />
            );
          })}
        </Switch>
        <GlobalStyle />
        <CommonGlobalStyle />
      </StyledAppInner>
    </>
  );
};
