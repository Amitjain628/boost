const isMobile = {
  get phone(): boolean {
    // tslint:disable-next-line:no-string-literal
    return window['isMobile'];
  }
};
export default isMobile;
